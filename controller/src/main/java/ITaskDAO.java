import model.Task;
import model.User;

/**
 * Created by Sasha on 14.03.2016.
 */
public interface ITaskDAO {
    public void addTask(Task task);
    public void updateTask(Task task);
    public Task getTaskById(int task_id);
    public void deleteTask(Task task);
    public void deleteTask(int id);
}