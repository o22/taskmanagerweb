import model.Task;
import model.TasksData;
import model.User;
import model.UsersData;
import org.hibernate.mapping.RootClass;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.*;
import java.util.List;

/**
 * Created by 0_o on 09.04.2016.
 */
public class XmlWorker {
    public static void encodeTask(Task task) {
        try {
            JAXBContext jc = JAXBContext.newInstance(Task.class);

            Marshaller m = jc.createMarshaller();
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = schemaFactory.newSchema(new StreamSource(new File("tmschema.xml")));

            m.setSchema(schema);

            m.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            OutputStream os = new FileOutputStream("test1.xml");

            m.marshal(task, os);

            os.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }


    }

    public static boolean validateXML(String xsdPath, String xmlPath) {

        try {
            SchemaFactory factory =
                    SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = factory.newSchema(new File(xsdPath));
            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(new File(xmlPath)));
        } catch (SAXException e) {
            System.out.println("Exception: " + e.getMessage());
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static void encodeUser(User user, String fileName) {
        FileOutputStream fos = null;
        try {
            JAXBContext jc = JAXBContext.newInstance(User.class);

            Marshaller m = jc.createMarshaller();
            m.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            OutputStream os = new FileOutputStream(fileName + ".xml");

            m.marshal(user, os);

            os.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }


    }

    public static void encodeUsers(List<User> users,String fileName) {
        FileOutputStream fos = null;
        try {
            UsersData ud = new UsersData();
            ud.setUsers(users);
            JAXBContext jc = JAXBContext.newInstance(UsersData.class);

            Marshaller m = jc.createMarshaller();
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = schemaFactory.newSchema(new StreamSource(new File("tmschema.xsd")));

           m.setSchema(schema);
            m.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            OutputStream os = new FileOutputStream(fileName+".xml");

            m.marshal(ud, os);

            os.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }


    }

    public static File encodeTasks(List<Task> tasks, String fileName) {
        FileOutputStream fos = null;
        try {
            TasksData td = new TasksData();
            td.setTasks(tasks);
            JAXBContext jc = JAXBContext.newInstance(TasksData.class);

            Marshaller m = jc.createMarshaller();

            //SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            //Schema schema = schemaFactory.newSchema(new StreamSource(new File("tasksscheme.xsd")));

            //m.setSchema(schema);
            //m.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            OutputStream os = new FileOutputStream(fileName + ".xml");

            m.marshal(td, os);

            os.close();
            return new File(fileName + ".xml");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("d1");
        } catch (IOException e) {
            System.out.println("d2");
            e.printStackTrace();
        } catch (JAXBException e) {
            System.out.println("d3");
            e.printStackTrace();
        }
return null;

    }

    public static Task decodeTask(String name) {
        FileInputStream fis = null;
        try {
            JAXBContext jc = JAXBContext.newInstance(Task.class);

            InputStream is = new FileInputStream("test1.xml");

            Unmarshaller um = jc.createUnmarshaller();

            Task task = (Task) um.unmarshal(is);

            is.close();
            return task;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return null;


    }

    public static User decodeUser(String name) {
        FileInputStream fis = null;
        try {
            JAXBContext jc = JAXBContext.newInstance(User.class);

            InputStream is = new FileInputStream("user.xml");

            Unmarshaller um = jc.createUnmarshaller();

            User user = (User) um.unmarshal(is);

            is.close();
            return user;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return null;


    }

    public static List<User> decodeUsers(String fileName) {
        FileInputStream fis = null;
        try {
            JAXBContext jc = JAXBContext.newInstance(UsersData.class);

            InputStream is = new FileInputStream(fileName + ".xml");

            Unmarshaller um = jc.createUnmarshaller();
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = schemaFactory.newSchema(new StreamSource(new File("tmschema.xsd")));

            //um.setSchema(schema);

            UsersData data = (UsersData) um.unmarshal(is);

            is.close();
            return data.getUsers();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }

        return null;
    }


    public static List<Task> decodeTasks(String fileName) {
        FileInputStream fis = null;
        try {
            JAXBContext jc = JAXBContext.newInstance(TasksData.class);

            InputStream is = new FileInputStream(fileName + ".xml");

            Unmarshaller um = jc.createUnmarshaller();
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = schemaFactory.newSchema(new StreamSource(new File("tasksscheme.xsd")));

            um.setSchema(schema);

            TasksData data = (TasksData) um.unmarshal(is);

            is.close();
            return data.getTasks();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }

        return null;
    }
    public static List<Task> decodeTasks(File file) {
        FileInputStream fis = null;
        try {
            JAXBContext jc = JAXBContext.newInstance(TasksData.class);

            InputStream is = new FileInputStream(file);

            Unmarshaller um = jc.createUnmarshaller();
           // SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            //Schema schema = schemaFactory.newSchema(new StreamSource(new File("tasksscheme.xsd")));

            //um.setSchema(schema);

            TasksData data = (TasksData) um.unmarshal(is);

            is.close();
            return data.getTasks();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }

        return null;
    }


}
