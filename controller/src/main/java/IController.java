/**
 * Created by Sasha on 15.03.2016.
 */

import model.Task;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.Set;

/**
 * Created by Александр on 25.10.2015.
 */
public interface IController {

    public void addTask(Task task);


    public void deleteTask(Task task);

}