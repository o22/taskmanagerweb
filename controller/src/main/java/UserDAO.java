import model.User;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sasha on 14.03.2016.
 */
public class UserDAO {
    public void addUser(User user) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.out.println("Error");
        } finally {
            if (session != null && session.isOpen()) {

                session.close();
            }

        }
    }

    public void updateUser(User user) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.out.println("Error");
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public User checkUser(String login, String password) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            String hql = "from User where login = :login";
            Query query = session.createQuery(hql);
            query.setString("login", login);
            List<User> results = (List<User>) query.list();
            if (results != null && results.size() != 0)
                return results.get(0);

        } catch (Exception e) {
            System.out.println("Error");
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return null;
    }

    public void reloadUser(User user) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.refresh(user);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.out.println("Error");
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public List<User> getEmploes(User user) {
        Session session = null;
        List<User> resultsUser = new ArrayList<User>();
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            String hql = "from User where boss = :id";
            Query query = session.createQuery(hql);
            query.setString("id", user.getId().toString());
            resultsUser = (List<User>) query.list();
        } catch (Exception e) {
            System.out.println("Error");
        } finally {
            if (session != null && session.isOpen()) {

                session.close();
            }

        }
        return resultsUser;
    }
    public List<User> getAllUsers() {
        Session session = null;
        List<User> resultsUser = new ArrayList<User>();
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            String hql = "from User";
            Query query = session.createQuery(hql);
            resultsUser = (List<User>) query.list();
        } catch (Exception e) {
            System.out.println("Error");
        } finally {
            if (session != null && session.isOpen()) {

                session.close();
            }

        }
        return resultsUser;
    }

    public User getUserById(int user_id) {
        Session session = null;
        User user = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            user = (User) session.get(User.class, user_id);
        } catch (Exception e) {

        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return user;
    }

    public void deleteUser(User user) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.delete(user);
            session.getTransaction().commit();
        } catch (Exception e) {
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

    }
}
