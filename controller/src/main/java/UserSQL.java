import model.Task;
import model.User;
import org.hibernate.Query;
import org.hibernate.Session;

import javax.naming.NamingException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sasha on 14.03.2016.
 */
public class UserSQL {

    private Connection getConnection() throws SQLException, NamingException, ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");
        return DriverManager.getConnection("jdbc:mysql://localhost:3306/taskmanager", JDBC.username, JDBC.password);
    }

    public static final String INSERT = "INSERT INTO `taskmanager`.`users` " +
            "(`name`, `login`,`password`,`boss`) " +
            "VALUES (?,?,?,?)";
    public static final String UPDATE = "UPDATE `taskmanager`.`users` " +
            "SET `name`=?, `login`=?, `password`=?, `boss`=? WHERE id=?";
    public static final String DELETE = "DELETE FROM `taskmanager`.`users` WHERE `id`=?";

    public static final String FIND_ONE_QUERY = "SELECT `id`, `name`, `login`,`password`,`boss` " +
            "FROM `taskmanager`.`users` WHERE id=?";
    public static final String CHECK = "SELECT `id`, `name`, `login`,`password`,`boss` " +
            "FROM `taskmanager`.`users` WHERE login=? AND password=?";

    public static final String FIND_ALL_QUERY = "SELECT `id`, `name`, `login`,`password`,`boss` " +
            "FROM `taskmanager`.`users` WHERE boss=?" ;
    public static final String FIND_ALL_USERS = "SELECT `id`, `name`, `login`,`password`,`boss` " +
            "FROM `taskmanager`.`users`" ;
    public void addUser(User user) {
        try (Connection connection = getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT);
            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getLogin());
            preparedStatement.setString(3, user.getPassword());
            preparedStatement.setInt(4, user.getBoss());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateUser(User user) {
        try (Connection connection = getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getLogin());
            preparedStatement.setString(3, user.getPassword());
            preparedStatement.setInt(4, user.getBoss());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public User checkUser(String login, String password) {
        try (Connection connection = getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(CHECK);
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, password);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            User user = new User(resultSet.getInt("boss"),
                    resultSet.getString("name"),resultSet.getString("password"),resultSet.getString("login"));
            user.setId(resultSet.getInt("id"));

            //////
            PreparedStatement tasks_statement = connection.prepareStatement
                    ("SELECT `id`, `name`, `description`,`dateStr`,`contacts`,`userid` " +
                    "FROM `taskmanager`.`tasks` WHERE userid=?");
            tasks_statement.setInt(1, user.getId());
            ResultSet resultTasks = tasks_statement.executeQuery();
            List<Task> arrayList = new ArrayList<>();
            while (resultTasks.next()) {
                Task task= new Task(resultTasks.getString("name"),
                        resultTasks.getString("description"),resultTasks.getString("dateStr"),resultTasks.getString("contacts"),user.getId());
                task.setId(resultTasks.getInt("id"));
                arrayList.add(task);
            }
            user.setTasks(arrayList);
            return user;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }



    public List<User> getEmploes(User user) {
        try (Connection connection = getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(FIND_ALL_QUERY);
            preparedStatement.setInt(1, user.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            List<User> arrayList = new ArrayList<>();
            while (resultSet.next()) {
                arrayList.add(new User(user.getId(),resultSet.getString("name"),
                        resultSet.getString("password"),resultSet.getString("login")));
                user.setId(resultSet.getInt("id"));
            }
            return arrayList;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public List<User> getAllUsers() {
        try (Connection connection = getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(FIND_ALL_USERS);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<User> arrayList = new ArrayList<>();
            while (resultSet.next()) {
                User u = new User(resultSet.getInt("boss"),
                        resultSet.getString("name"),resultSet.getString("password"),resultSet.getString("login"));
                u.setId(resultSet.getInt("id"));
                arrayList.add(u);

            }
            return arrayList;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public User getUserById(int user_id) {
        try (Connection connection = getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(FIND_ONE_QUERY);

            preparedStatement.setInt(1, user_id);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            User user = new User(resultSet.getInt("boss"),
                    resultSet.getString("name"),resultSet.getString("password"),resultSet.getString("login"));
            user.setId(resultSet.getInt("id"));
            return user;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void deleteUser(User user) {
        try (Connection connection = getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE);
            preparedStatement.setInt(1, user.getId());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
