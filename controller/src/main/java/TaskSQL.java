import model.Task;
import model.User;
import org.hibernate.Query;
import org.hibernate.Session;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sasha on 14.03.2016.
 */
public class TaskSQL implements ITaskDAO {

    private Connection getConnection() throws SQLException, NamingException, ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");
        return DriverManager.getConnection("jdbc:mysql://localhost:3306/taskmanager", JDBC.username, JDBC.password);
    }

    public static final String INSERT = "INSERT INTO `taskmanager`.`tasks` " +
            "(`name`, `description`,`dateStr`,`contacts`,`userid`) " +
            "VALUES (?,?,?,?,?)";
    public static final String UPDATE = "UPDATE `taskmanager`.`tasks` " +
            "SET `name`=?, `description`=?, `dateStr`=?, `contacts`=?, `userid`=? WHERE id=?";
    public static final String DELETE = "DELETE FROM `taskmanager`.`tasks` WHERE `id`=?";

    public static final String FIND_ONE_QUERY = "SELECT `id`, `name`, `description`,`dateStr`,`contacts`,`userid` " +
            "FROM `taskmanager`.`tasks` WHERE id=?";
    public static final String FIND_ALL_QUERY = "SELECT `id`, `name`, `description`,`dateStr`,`contacts`,`userid` FROM `taskmanager`.`tasks` WHERE userid=?" ;
    public static final String FIND_ALL_TASKS = "SELECT `id`, `name`, `description`,`dateStr`,`contacts`,`userid` " +
            "FROM `taskmanager`.`tasks`" ;
    public static final String FIND_USERS = "SELECT `id`, `name`, `login`,`password`,`boss` " +
            "FROM `taskmanager`.`users` WHERE boss=?" ;
    public void addTask(Task task) {
        try (Connection connection = getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT);
            preparedStatement.setString(1, task.getName());
            preparedStatement.setString(2, task.getDescription());
            preparedStatement.setString(3, task.getDateStr());
            preparedStatement.setString(4, task.getContacts());
            preparedStatement.setInt(5, task.getUserID());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void updateTask(Task task) {
        try (Connection connection = getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
            preparedStatement.setInt(6, task.getId());
            preparedStatement.setString(1, task.getName());
            preparedStatement.setString(2, task.getDescription());
            preparedStatement.setString(3, task.getDateStr());
            preparedStatement.setString(4, task.getContacts());
            preparedStatement.setInt(5, task.getUserID());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Task> getUserTasks(int user) {
        try (Connection connection = getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(FIND_ALL_QUERY);
            preparedStatement.setInt(1, user);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<Task> arrayList = new ArrayList<>();
            while (resultSet.next()) {
                arrayList.add(new Task(resultSet.getInt("id"),resultSet.getString("name"),
                        resultSet.getString("description"),resultSet.getString("dateStr"),resultSet.getString("contacts"),user));
            }
            return arrayList;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public List<Task> getAllTasks() {
        try (Connection connection = getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(FIND_ALL_TASKS);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<Task> arrayList = new ArrayList<>();
            while (resultSet.next()) {
                Task t = new Task(resultSet.getInt("id"),resultSet.getString("name"),
                        resultSet.getString("description"),resultSet.getString("dateStr"),resultSet.getString("contacts"));

                arrayList.add(t);
            }
            return arrayList;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }



    public List<Task> getTasksEmploes(User user) {
        try (Connection connection = getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(FIND_USERS);
            preparedStatement.setInt(1, user.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            List<Task> arrayList = new ArrayList<>();
            List<User> tmp = new ArrayList<>();
            while (resultSet.next()) {
                User u = new User(user.getId(),resultSet.getString("name"),
                        resultSet.getString("password"),resultSet.getString("login"));
                u.setId(resultSet.getInt("id"));
                tmp.add(u);
            }
            for(User us:tmp){
                arrayList.addAll(getUserTasks(us.getId()));

            }
            return arrayList;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Task getTaskById(int task_id) {
        try (Connection connection = getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(FIND_ONE_QUERY);
            preparedStatement.setInt(1, task_id);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            Task task= new Task(resultSet.getString("name"),
                    resultSet.getString("description"),resultSet.getString("dateStr"),resultSet.getString("contacts"),resultSet.getInt("userid"));
            task.setId(resultSet.getInt("id"));
            return task;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void deleteTask(Task task) {

    }


    public void deleteTask(int id) {
        try (Connection connection = getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE);
            preparedStatement.setInt(1, id);

            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
