import model.Task;
import model.User;

import java.util.ArrayList;
import java.util.List;

public class ControllerSQL implements IController {

    private User currentUser;
    private UserSQL userDao;
    private TaskSQL taskDao;
    private List<Task> currentTasks=new ArrayList<Task>();
    private static ControllerSQL instance;
    private Task editTask;

    public Task getEditTask() {
        return editTask;
    }

    public void setEditTask(Task editTask) {
        this.editTask = editTask;
    }

    public static synchronized ControllerSQL getInstance() {
        if (instance == null) {
            instance = new ControllerSQL();
        }
        return instance;
    }

    private ControllerSQL() {
        userDao = new UserSQL();
        taskDao = new TaskSQL();
    }


    /**
     * Авторизация
     *
     * @param login
     * @param password
     * @return
     */
    public boolean checkAuth(String login, String password) {
        User us = userDao.checkUser(login, password);
        if (us != null) {
            setCurrentUser(us);
            return true;
        } else
            return false;
    }

    public void setCurrentUser(User user) {
        this.currentUser = user;
        this.currentTasks=user.getTasks();
    }

    public User getCurrentUser() {
        return currentUser;
    }

    /**
     * Регистрация через текущего пользователья
     *
     * @param login
     * @param password
     * @return
     */
    public boolean registerUser(String login,String name ,String password) {
        User newUser = new User(this.currentUser.getId(),name, login, password);
        return addUser(newUser);
    }
    /**
     * Регистрация через указание id босса
     *
     * @param login
     * @param password
     * @param id
     * @return
     */
    public boolean registerUser(String login,String name, String password, int id) {
        User newUser = new User(id, name, login, password);
        return addUser(newUser);
    }
    public List<User> getAllUsers(){
        return userDao.getAllUsers();
    }
    public List<Task> getAllTasks(){
        return taskDao.getAllTasks();

    }
    public void saveUsers(List<User> usersList){
        for(User user : usersList){
            List<Task> userTasks = new ArrayList<Task>();
            userTasks = user.getTasks();
            user.setTasks(new ArrayList<Task>());
            //saveTasks(userTasks,user);
            if(getUserById(user.getId())==null) {

                userDao.addUser(user);
            }
            else{

                userDao.updateUser(user);
            }



        }

    }
    public void saveTasks(List<Task> tasksList,boolean rewrite){
        for(Task task : tasksList){
            Task t = new Task(task.getId(),task.getName(),task.getDescription(),task.getDateStr(),task.getContacts(),currentUser.getId());

            if(getTaskById(task.getId())!=null&&rewrite){
                updateTask(t);
            }
            else if(getTaskById(task.getId())==null)
            addTask(t);



        }

    }
    public void saveTasks(List<Task> tasksList,User user,boolean rewrite){
        for(Task task : tasksList){
            Task t = new Task(task.getId(),task.getName(),task.getDescription(),task.getDateStr(),task.getContacts(),user.getId());
            if(getTaskById(task.getId())==null&&rewrite)
                addTask(t);
            else
                updateTask(t);

        }

    }




    /**
     * @return Список подчиненных
     */
    public List<User> getEmploes() {
        return userDao.getEmploes(currentUser);

    }

    /**
     * @return Список задач подчиненных
     */
    public List<Task> getTasksEmploes() {
        return taskDao.getTasksEmploes(currentUser);

    }


    /**
     * Изменение задачи
     *
     * @param task
     */
    public void updateTask(Task task) {
        System.out.println("Start update");
        taskDao.updateTask(task);
        System.out.println("Finish");
        for(int i = 0;i<currentUser.getTasks().size();i++){
            if(task.getId()==currentUser.getTasks().get(i).getId()){
                currentUser.getTasks().set(i,task);
            }
        }
        currentTasks=currentUser.getTasks();

    }

    /**
     * Добавление задачи
     *
     * @param task
     */
    public void addTask(Task task){
        taskDao.addTask(task);
        currentUser.addTask(task);
        currentTasks=currentUser.getTasks();

    }



    /**
     * Добавление нового пользователя без проверок
     *
     * @param user
     * @return
     */
    public boolean addUser(User user) {
        if (userDao.checkUser(user.getLogin(), user.getPassword()) == null) {
            userDao.addUser(user);
            return true;
        } else
            return false;

    }

    public User getUserById(int id) {
        return userDao.getUserById(id);

    }

    /**
     * @return Вывод всех задач пользователя
     */
    public List<Task> getUserTasks() {
        return currentUser.getTasks();
    }

    public List<Task> getCurrentTasks() {
        return currentTasks;
    }

    public void deleteTask(Task task) {
        taskDao.deleteTask(task);
        currentTasks=currentUser.getTasks();

    }
    public List<Task> getTasksByName(String name){
        List<Task> res=new ArrayList<Task>();
        for(Task task:currentUser.getTasks()){
            System.out.println(task.getName() +"  "+ name);
            if(task.getName().toLowerCase().contains(name.toLowerCase())) {
                res.add(task);
                System.out.println(task.getName());
            }
        }
        currentTasks=res;
        return res;

    }
    public void deleteTaskById(int id){
        taskDao.deleteTask(id);
        currentUser.setTasks(taskDao.getUserTasks(currentUser.getId()));
        currentTasks=currentUser.getTasks();

    }
    public void deleteUserById(int id){
        User u = getUserById(id);
        for(Task t:u.getTasks()){
            taskDao.deleteTask(t);
        }
        userDao.deleteUser(u);

    }


    public Task getTaskById(int id) {
        return taskDao.getTaskById(id);

    }


}

