import model.Task;
import model.User;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/**
 * Created by Sasha on 08.05.2016.
 */
public class SQLTest {
    @Test
    public void testCheckAuth() throws Exception {
        ControllerSQL controller = ControllerSQL.getInstance();

        assertEquals(controller.checkAuth("test","test"), true);
    }
    @Test
    public void testRegister() throws Exception{
        ControllerSQL controller = ControllerSQL.getInstance();

        controller.registerUser("test","test","test");
    }
    @Test
    public void testGetTaskById() throws Exception {
        ControllerSQL controller = ControllerSQL.getInstance();

        System.out.println(controller.getTaskById(7).toString());
    }
    @Test
    public void testUpdateTaskById() throws Exception {
        ControllerSQL controller = ControllerSQL.getInstance();
        controller.checkAuth("test","test");
        User me = controller.getCurrentUser();
        System.out.println(me.getTasks().size());
        Task t = new Task("task12", "asdaddd", "hhh", "asdddddddddasd",me.getId());
        Task t2 = new Task("aasd", "asdsad", "", "asdasd", me.getId());
        controller.addTask(t);
        controller.addTask(t2);

        System.out.println(me.getTasks().size());

    }
    @Test
    public void testGetTasksEmploes() throws Exception {
        ControllerSQL controller = ControllerSQL.getInstance();
        controller.checkAuth("test","test");
        for(Task t:controller.getTasksEmploes()){
            System.out.println(t.toString());

        }

        //assertEquals(controller.addUser(new User("test","test")), false);
    }
    @Test
    public void testFillTasks()throws Exception{
        ControllerSQL controller = ControllerSQL.getInstance();
        controller.checkAuth("test","test");
        User me = controller.getCurrentUser();
        Task t = new Task("asd", "asdad", "", "asdasd", me.getId());

        Task t2 = new Task("aasd", "asdsad","", "asdasd", me.getId());
        controller.addTask(t);
        controller.addTask(t2);

        //User me = controller.getUserById(2);
        for(Task task:controller.getUserTasks()){
            System.out.println(task.toString());
        }

    }
    @Test
    public void testAddTasks()throws Exception{
        ControllerSQL controller = ControllerSQL.getInstance();
        controller.checkAuth("test","test");
        User me = controller.getCurrentUser();


        //User me = controller.getUserById(2);
        for(Task task:controller.getUserTasks()){
            System.out.println(task.toString());
        }

    }

}
