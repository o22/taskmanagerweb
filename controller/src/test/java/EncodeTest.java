import model.Task;
import model.User;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

/**
 * Created by 0_o on 09.04.2016.
 */
public class EncodeTest {
    @Test
    public void testEncodeTask(){
        Task task = new Task("Name","Description","Date","Contacts");
        XmlWorker.encodeTask(task);

    }
    @Test
    public void testEncodeUser(){
        List<Task> tasks = new ArrayList<Task>();
        Task task1 = new Task("Name","Description","Date","Contacts");
        Task task2 = new Task("Name2","Description","Date","Contacts");
        Task task3 = new Task("Name3","Description","Date","Contacts");
        tasks.add(task1);
        tasks.add(task2);
        tasks.add(task3);
        User u = new User("test","test");
        u.setTasks(tasks);
        XmlWorker.encodeUser(u,"test");

    }
    @Test
    public void testDecodeTask(){
        Task task = XmlWorker.decodeTask("asddas");
        assertNotNull(task);
    }
    @Test
    public void testDecodeTasks(){
        List<Task> tasks = new ArrayList<Task>();
        Task task1 = new Task("Name","Description","Date","Contacts");
        Task task2 = new Task("Name2","Description","Date","Contacts");
        Task task3 = new Task("Name3","Description","Date","Contacts");
        tasks.add(task1);
        tasks.add(task2);
        tasks.add(task3);

        XmlWorker.encodeTasks(tasks,"sfsd");
    }
    @Test
    public void testEncodeUsers(){
        List<User> users = new ArrayList<User>();
        List<Task> tasks = new ArrayList<Task>();
        Task task1 = new Task("Name","Description","Date","Contacts");
        Task task2 = new Task("Name2","Description","Date","Contacts");
        Task task3 = new Task("Name3","Description","Date","Contacts");
        tasks.add(task1);
        tasks.add(task2);
        tasks.add(task3);
        User u = new User("test","test");
        u.setTasks(tasks);
        users.add(u);
        users.add(u);



        XmlWorker.encodeUsers(users,"users");

    }
    @Test
    public void testValidate(){
        assertEquals(XmlWorker.validateXML("task.xsd","task.xml"),true);
    }
}
