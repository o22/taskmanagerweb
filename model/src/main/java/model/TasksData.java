package model;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 0_o on 11.04.2016.
 */
@XmlRootElement
@XmlSeeAlso({Task.class})
public class    TasksData {
    private List<Task> tasks;

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public void addUser(Task user) {
        if (tasks == null)
            tasks = new ArrayList<Task>();
        tasks.add(user);
    }
}