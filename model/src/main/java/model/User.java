package model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Sasha on 14.03.2016.
 */
@Entity
@Table(name="Users",uniqueConstraints={@UniqueConstraint(columnNames={"id"})})
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class User implements Serializable {
    @Id
    @GeneratedValue(generator="increment")
    @GenericGenerator(name="increment", strategy = "increment")
    @Column(name="id", nullable=false)
    @XmlElement
    private Integer id;
    @Column(name="name", nullable=true)
    @XmlElement
    private String name;
    @Column(name="login", nullable=false)
    @XmlElement
    private String login;
    @Column(name="password", nullable=false)
    private String password;
    @Column(name="boss", nullable=true)
    private Integer boss;

    public String getLogin() {
        return login;
    }

    public User(String login, String password) {
        this.login = login;
        this.password = password;
        this.boss=0;

    }
    public User(){
        tasks = new ArrayList<Task>();

    }
    public User(Integer boss, String password, String login) {
        this.boss = boss;
        this.password = password;
        this.login = login;
    }
    public User(Integer boss,String name, String password, String login) {
        this.boss = boss;
        this.password = password;
        this.login = login;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getBoss() {
        return boss;
    }

    public void setBoss(Integer boss) {
        this.boss = boss;
    }
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy="userID")
    private List<Task> tasks;


    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }


    public void addTask(Task task) {
        if (tasks == null)
            tasks = new ArrayList<Task>();
        tasks.add(task);

    }
    public boolean deleteTask(Task task) {
        return tasks.remove(task);
    }

}
