import model.Task;
import model.TasksData;
import model.User;
import model.UsersData;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SaveCommand implements Command {
    public String execute(HttpServletRequest request, HttpServletResponse response) throws SQLException, NoSuchAlgorithmException {
        ControllerSQL c = ControllerSQL.getInstance();





        try {
            String mode = request.getParameter("mode");
                String fileName = request.getParameter("file");
                List<Task> arrayList = c.getCurrentTasks();
                File file = XmlWorker.encodeTasks(arrayList, fileName);
                OutputStream out = response.getOutputStream();
                FileInputStream in = new FileInputStream(file);
                byte[] buffer = new byte[4096];
                int length;
                while ((length = in.read(buffer)) > 0){
                    out.write(buffer, 0, length);
                }
                in.close();
                out.flush();

            /*if(mode.equals("user")) {
                String fileName = request.getParameter("file");
                XmlWorker.encodeUser(c.getCurrentUser(), fileName);
            }
            if(mode.equals("all")) {
                String fileName = request.getParameter("file");
                XmlWorker.encodeUsers(c.getAllUsers(),fileName);


            }
            if(mode.equals("loadfile")){
                System.out.println("asasasas");
               String   f = (String) request.getAttribute("file");
                System.out.println(f);
               TasksData ud =new TasksData();
               ud.setTasks(XmlWorker.decodeTasks(f));
               c.saveTasks(ud.getTasks());



            }
            if(mode.equals("loadTasks")){
                TasksData ud =new TasksData();
                ud.setTasks(XmlWorker.decodeTasks(request.getParameter("file")));
                c.saveTasks(ud.getTasks());


            }
            if(mode.equals("loadUsers")) {
                UsersData ud = new UsersData();
                ud.setUsers(XmlWorker.decodeUsers(request.getParameter("file")));
                c.saveUsers(ud.getUsers());


            }*/


                request.setAttribute("success", "success");

        }
        catch (Exception e){
            request.setAttribute("success", "error");
        }


        return "/resources/include/table.jsp";


    }
}
