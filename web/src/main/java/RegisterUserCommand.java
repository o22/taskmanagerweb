import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

/**
 * Created by Nastya on 21.03.2016.
 */
public class RegisterUserCommand implements Command {
    public String execute(HttpServletRequest request, HttpServletResponse response) throws SQLException, NoSuchAlgorithmException {
        String page = null;
        String user = request.getParameter("login");
        page = "/TaskManager?command=show";
        try {
            ControllerSQL c = ControllerSQL.getInstance();

            String password = request.getParameter("password");
            String name = request.getParameter("name");
            String boss = request.getParameter("boss");
                int bossInt = Integer.parseInt(boss);


                if (c.registerUser(user, name, password, bossInt))
                    request.setAttribute("success", "success");

        } catch (Exception e) {
            ControllerSQL c = ControllerSQL.getInstance();
            String password = request.getParameter("password");
            String name = request.getParameter("name");
            if (c.registerUser(user, name, password))
                request.setAttribute("success", "success");
        }

        return page;
    }
}
