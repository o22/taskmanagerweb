/**
 * Created by Sasha on 09.05.2016.
 */
import model.Task;
import model.TasksData;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import java.io.*;
import java.util.List;
import java.util.Iterator;

public class FileUploadDemoServle extends HttpServlet {
    private static final long serialVersionUID = -3208409086358916855L;
    public void loadTasks(File file,boolean rewrite ){
        ControllerSQL c = ControllerSQL.getInstance();
        TasksData ud =new TasksData();
        ud.setTasks(XmlWorker.decodeTasks(file));
        c.saveTasks(ud.getTasks(),rewrite);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);

        if (isMultipart) {
            FileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);

            try {
                List items = upload.parseRequest(request);
                Iterator iterator = items.iterator();
                while (iterator.hasNext()) {
                    FileItem item = (FileItem) iterator.next();

                    if (!item.isFormField()) {
                        String fileName = item.getName();
                        String root = getServletContext().getRealPath("/");
                        File path = new File(root + "/uploads");
                        if (!path.exists()) {
                            boolean status = path.mkdirs();
                        }

                        File uploadedFile = new File(path + "/" + fileName);
                        String a = request.getParameter("rewrite");
                        System.out.println(uploadedFile.getAbsolutePath());
                        item.write(uploadedFile);
                        loadTasks(uploadedFile,true);

                            loadTasks(uploadedFile,false);
                    }
                }
            } catch (FileUploadException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        ControllerSQL c = ControllerSQL.getInstance();
        List<Task> arrayList = c.getUserTasks();
        request.setAttribute("arrayList",arrayList);
        request.setAttribute("mode_emp",0);
        request.setAttribute("user_id",c.getCurrentUser().getId());
        request.getRequestDispatcher("/resources/include/table.jsp").forward(request, response);

    }
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws IOException{
        String mode = request.getParameter("mode");
        ControllerSQL c = ControllerSQL.getInstance();
        if(mode.equals("tasks")) {
            String fileName = request.getParameter("file");
            List<Task> arrayList = c.getCurrentTasks();
            File file = XmlWorker.encodeTasks(arrayList, fileName);
            File                f        = file;
            int                 length   = 0;
            ServletOutputStream op       = response.getOutputStream();
            ServletContext      context  = getServletConfig().getServletContext();
            String              mimetype = context.getMimeType( fileName );
            //
            //  Set the response and go!
            //
            //
            response.setContentType( (mimetype != null) ? mimetype : "application/octet-stream" );
            response.setContentLength( (int)f.length() );
            response.setHeader( "Content-Disposition", "attachment; filename=\"" + fileName+".xml" + "\"" );
            //
            //  Stream to the requester.
            //
            byte[] bbuf = new byte[1024];
            DataInputStream in = new DataInputStream(new FileInputStream(f));
            while ((in != null) && ((length = in.read(bbuf)) != -1))
            {
                op.write(bbuf,0,length);
            }
            in.close();
            op.flush();
            op.close();
        }
    }
}