import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

/**
 * Created by Nastya on 21.03.2016.
 */
public class DeleteTaskCommand implements Command {
    public String execute(HttpServletRequest request, HttpServletResponse response) throws SQLException, NoSuchAlgorithmException {
        try {
            String taskID = request.getParameter("taskID");
            ControllerSQL c = ControllerSQL.getInstance();

            c.deleteTaskById(Integer.parseInt(taskID));
            request.setAttribute("success", "success");
        }
        catch (NumberFormatException e){
            String taskID = request.getParameter("userID");
            ControllerSQL c = ControllerSQL.getInstance();

            c.deleteUserById(Integer.parseInt(taskID));
            request.setAttribute("success", "success");
        }
        return "/TaskManager?command=show";
    }
}
