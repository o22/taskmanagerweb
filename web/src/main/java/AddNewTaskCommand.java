import model.Task;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.concurrent.ExecutionException;

/**
 * Created by Nastya on 21.03.2016.
 */
public class AddNewTaskCommand implements Command {
    public String execute(HttpServletRequest request, HttpServletResponse response) throws SQLException, NoSuchAlgorithmException {
        String email = (String) request.getSession().getAttribute("user");
        String name =  request.getParameter("name");
        String description =  request.getParameter("description");
        String date = request.getParameter("date");
        String contacts = request.getParameter("contacts");
        try {
            if(name!=null&& !name.replaceAll(" ", "").equals("")) {

                ControllerSQL c = ControllerSQL.getInstance();
                Task task = new Task(name, description, date, contacts, c.getCurrentUser().getId());
                c.addTask(task);

                request.setAttribute("success", "success");
            }
            else
                request.setAttribute("success", "error");
        }
        catch (Exception e){
            request.setAttribute("success", "error");
        }
        return "/TaskManager?command=show";
    }
}
