import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

/**
 * Created by Nastya on 21.03.2016.
 */
public class AuthCommand implements Command {
    public String execute(HttpServletRequest request, HttpServletResponse response) throws SQLException, NoSuchAlgorithmException {
        String page = null;
        String user = request.getParameter("name");
        ControllerSQL c = ControllerSQL.getInstance();
        if(user == null){
            request.setAttribute("error","enter your login please :(");
            return "index.jsp";
        }
        String password = request.getParameter("password");
        if(c.checkAuth(user,password)) {
            request.getSession().setAttribute("user", user);
            request.setAttribute("success","success");
            page = "/TaskManager?command=show";
        }
        else{
            request.setAttribute("error","Please, enter correct password ");
            page = "index.jsp";
        }
        return page;
    }
}
