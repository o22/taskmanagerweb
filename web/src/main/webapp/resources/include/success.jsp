<%--
  Created by IntelliJ IDEA.
  User: Nastya
  Date: 20.03.2016
  Time: 19:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="java.util.Enumeration" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html lang="en">
<head></head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../favicon.ico">

<title>Sign in</title>

<!-- Bootstrap core CSS -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet">
<link href="resources/css/signin.css" rel="stylesheet">
<link rel="stylesheet" href="resources/css/datepicker.css">
<script src="resources/js/bootstrap.min.js" type="text/javascript" ></script>
<script type="text/javascript" src="resources/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="resources/js/bootstrap-datepicker.js"></script>

<%--Navbar panel--%>
<c:redirect url="/TaskManager?command=show">
</c:redirect>
</html>