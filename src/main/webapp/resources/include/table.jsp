<%--
  Created by IntelliJ IDEA.
  User: Nastya
  Date: 20.03.2016
  Time: 19:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="java.util.Enumeration" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Sign in</title>

    <!-- Bootstrap core CSS -->
    <link href="resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="resources/js/bootstrap.min.js">
    <link href="resources/css/signin.css" rel="stylesheet">
    <script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="resources/js/bootstrap.min.js"></script>


</head>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><%= session.getAttribute("user")%></a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="?command=show">Home</a></li>
                <li class=""><a href="?command=add">Add</a></li>
                <li><a href="?command=logout">logout</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>
<div class="container">
    <div class="row">


        <div class="col-md-12">
            <br>
            <h4><%
                String error = (String) request.getAttribute("error");
                if(error!=null) {out.print(error);}
            %></h4>
            <h2>TaskManager</h2>
            <div class="table-responsive">


                <table id="mytable" class="table table-bordred table-striped">

                    <thead>

                    <%--<th><input type="checkbox" id="checkall" /></th>--%>
                    <th>Date</th>
                    <th>Subject</th>
                    <th>Description</th>

                    <th>Edit</th>

                    <th>Delete</th>
                    </thead>
                    <tbody>


                    <c:forEach var="row" items="${arrayList}">
                        <tr>
                                <%--<td><input type="checkbox" class="checkthis" /></td>--%>
                            <c:forEach var="cell" items="${row}">
                                <c:choose>
                                    <c:when test="${cell.key=='id'}">
                                        <%--<input type="hidden" name="eventID" value="${cell.value}">--%>
                                        <td><p data-placement="top" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-xs announce" data-title="Edit" data-toggle="modal" data-target="#edit" onclick="editEvent(${cell.value})"><span class="glyphicon glyphicon-pencil"></span></button></p></td>
                                        <td><p data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" onclick="deleteEvent(${cell.value})" ><span class="glyphicon glyphicon-trash"></span></button></p></td>
                                    </c:when>
                                    <c:otherwise>
                                        <td>${cell.value}</td>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>


                        </tr>
                    </c:forEach>
                    </tbody>
                </table>


            </div>
        </div>
    </div>


    <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="TaskManager" method="post">
                    <input type="hidden" name="eventID" id="eventIdInput">
                    <input type="hidden" name="command" value="editEvent">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                        <h4 class="modal-title custom_align" id="Heading">Edit Your Event</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <input name="subject" id="" class="form-control " type="text" placeholder="subject">
                        </div>
                        <div class="form-group">

                            <input class="form-control " name="date" type="text" placeholder="date">
                        </div>
                        <div class="form-group">
                            <textarea rows="2" name="description" class="form-control" placeholder="Description"></textarea>


                        </div>
                    </div>
                    <div class="modal-footer ">
                        <button type="submit" class="btn btn-warning btn-lg" style="width: 100%;"><span class="glyphicon glyphicon-ok-sign"></span> Update</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>



    <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                    <h4 class="modal-title custom_align" id="Heading">Delete this entry</h4>
                </div>
                <div class="modal-body">

                    <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to delete this Record?</div>

                </div>
                <div class="modal-footer ">
                    <a id="deleteConfirm" href="?command=deleteEvent"><button type="button" class="btn btn-success" ><span class="glyphicon glyphicon-ok-sign"></span> Yes</button></a>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>
<script type="text/javascript">
    //        $(document).ready(function(){
    //            $(".announce").click(function(){ // Click to only happen on announce links
    //                $("#subject").val($(this).val("record_id"));
    //            });
    //        });
    //    $("#jstest").click(function(){
    //        alert("koks aaa");
    //    });
    function editEvent(id){
        document.getElementById('eventIdInput').value = id;
    }

    function deleteEvent(id){
        document.getElementById('deleteConfirm').setAttribute("href","?command=deleteEvent&eventID="+id);
    }

</script>
</html>