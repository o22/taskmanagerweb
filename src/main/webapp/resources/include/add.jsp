<%--
  Created by IntelliJ IDEA.
  User: Nastya
  Date: 20.03.2016
  Time: 19:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="java.util.Enumeration" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html lang="en">
<head></head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../favicon.ico">

<title>Sign in</title>

<!-- Bootstrap core CSS -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet">
<link href="resources/css/signin.css" rel="stylesheet">
<link rel="stylesheet" href="resources/css/datepicker.css">
<script src="resources/js/bootstrap.min.js" type="text/javascript" ></script>
<script type="text/javascript" src="resources/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="resources/js/bootstrap-datepicker.js"></script>

<%--Navbar panel--%>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><%= session.getAttribute("user")%></a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class=""><a href="?command=show">Home</a></li>
                <li class="active"><a href="?command=add">Add</a></li>
                <li><a href="?command=logout">logout</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>
<%--start add --%>

<div class="container">
    <div class="row">
        <div class="page-header">
            <h1>Describe your task</h1>
        </div>
        <form class="form-horizontal" role="form" method="post" action="TaskManager">
            <input type="hidden" name="command" value="addNew">
            <div class="form-group">
                <label for="Subject" class="col-sm-2 control-label">Subject :</label>
                <div class="col-sm-6">
                    <input type="text" name="subject" class="form-control" id="Subject" placeholder="Subject">
                </div>
            </div>
            <div class="form-group">
                <label for="Description" class="col-sm-2 control-label">Description :</label>
                <div class="col-sm-6">
                    <input type="text" name="description" class="form-control" id="Description" placeholder="Description">
                </div>
            </div>
            <div class="form-group">
                <label for="dob" class="col-sm-2 control-label">Date:</label>
                <div class="col-sm-6">
                    <input type="text" name="date" class="form-control datepicker" id="dob" placeholder="Event Date">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary" id="AddEvent" name="submit" value="Add">Add new Task</button>
                </div>
            </div>
        </form>
        <h4><%
            String error = (String) request.getAttribute("error");
            if(error!=null) {out.print(error);}
        %></h4>

    </div><!-- end for class "row" -->
</div><!-- end for class "container" -->
<%--end add --%>

</html>
